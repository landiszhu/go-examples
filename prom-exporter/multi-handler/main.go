package main

import (
	"fmt"
	//"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

var (
	// custom collector
	reg1 = prometheus.NewRegistry()
	// some metrics
	myGauge1 = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "metrics_for_collector_111",
			Help: "guage_help",
		},
		[]string{"l_111"},
	)

	reg2 = prometheus.NewRegistry()
	// some metrics
	myGauge2 = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "metrics_for_collector_222",
			Help: "guage_help",
		},
		[]string{"l_222"},
	)

	//reg3 *prometheus.Registry
)

func init() {
	// register metrics to my collector
	reg1.MustRegister(myGauge1, myGauge2)
	reg2.MustRegister(myGauge2)

}

func main() {
	// instrument
	var errHttpSvr1, errHttpSvr2 error
	errs := make(chan error, 2)
	//router := mux.NewRouter()

	myGauge1.WithLabelValues("l_111").Set(11111111)
	myGauge2.WithLabelValues("l_222").Set(22222222)


	handler1 := promhttp.HandlerFor(reg1, promhttp.HandlerOpts{})
	handler2 := promhttp.HandlerFor(reg2, promhttp.HandlerOpts{})

	// expose endpoint
	//http.Handle("/service1", handler1)
	http.Handle("/service1", handler1)

	http.Handle("/service2", handler2)
	go func(){
		errHttpSvr1 = http.ListenAndServe(":8101", nil)
		//errHttpSvr1 = http.ListenAndServe(":8101", handler1)

		if errHttpSvr1 != nil {
			fmt.Println("Http server 1 error:", errHttpSvr1)
		}
	} ()


	errHttpSvr2 = http.ListenAndServe(":8102", handler2)
	if errHttpSvr2 != nil {
		fmt.Println("Http server 2 error:", errHttpSvr2)
	}


	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT)
		errs <- fmt.Errorf("%s", <-c)
	}()

}

// https://gabrieltanner.org/blog/collecting-prometheus-metrics-in-golang