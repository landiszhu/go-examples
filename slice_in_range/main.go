package main
import "fmt"

func main() {
	var a = []int{2,4,6,12,9,7}
	var b = []int{4,6,7,11,5}
	for index_o, i := range a {
		found := false
		for _, j := range b {
			if i == j {
				found = true
				break
			}
		}
		// String not found. We add it to return slice
		fmt.Println("length b:",len(b))
		if !found {
			b = append(b, i)
			fmt.Println("append element index and value",index_o, i)
		}

	}
	fmt.Println("array a:",a,"\n array b:",b)
	fmt.Println("---------------------")
	k:=0
	for index_o, i := range b {
		found := false
		for _, j := range a {
			if i == j {
				found = true
				break
			}
		}
		// String not found. We add it to return slice
		fmt.Println("length b:",len(b))
		if !found {
			//NOTE: cannot remove it from the list here, or the next element will be skipped
			//b = append(b[:index_o], b[index_o+1:]...)
			fmt.Println("deleted element index and value",index_o, i)
		}else{
			b[k] =i
			k++
		}
	}
	fmt.Println("array a:",a,"\n array b:",b)
	//for p:=k;p<len(b);p++{
	//	b[p] = nil
	//}
	b=b[:k]
	fmt.Println("array a:",a,"\n array b:",b)
	fmt.Println("length b:",len(b))

}